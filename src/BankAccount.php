<?php

class BankAccount
{
    /**
     * @var float
     */
    protected $balance = 0.0;

    /**
     * @param $amount
     * @return float
     */
    public function deposit($amount)
    {
        return $this->balance + $amount;
    }
}