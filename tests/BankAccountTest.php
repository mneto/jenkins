<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mbneto
 * Date: 5/19/13
 * Time: 9:15 AM
 * To change this template use File | Settings | File Templates.
 */

class BankAccountTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var BankAccount
     */
    protected $bankAccount;

    public function setUp()
    {
        parent::setUp();
        $this->bankAccount = new \BankAccount();
    }

    public function testDeposit()
    {
        $this->assertEquals(1, $this->bankAccount->deposit(1));
    }
}
